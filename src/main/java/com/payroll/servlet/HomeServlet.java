package com.payroll.servlet;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;
import com.payroll.dataAccess.EmployeeDao;
import com.payroll.dataAccess.TimeCardDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HomeServlet", urlPatterns = {"home"}, loadOnStartup = 1)
public class HomeServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<HourlyEmployee> employees = EmployeeDao.getInstance().getHourlyEmployees();
        request.setAttribute("employees", employees);

        // page rendering
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("home.jsp");
        requestDispatcher.forward(request, response);
    }
}